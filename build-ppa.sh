#!/bin/bash
# I use this script to build and publish deb packages in ppa:mikhailnov/pulseeffects (https://launchpad.net/~mikhailnov/+archive/ubuntu/pulseeffects)
# I publish it to allow other people to use it and make it possible to maintain a new PPA easily in case I stop doing it for some reason
# I think, it can also be used for maintaining packages in mainline Debian (minor modifications required)

build_package(){
	#pkg_name="$(echo $directory | awk -F "-" '{print $1}')"
	#pkg_version="$(echo "$directory" | rev | cut -d "-" -f1 | rev)"
	#pkg_name="$(echo "$directory" | sed "s/"${pkg_version}-"//")"
	# https://stackoverflow.com/a/2899039
	pkg_name="$(echo "$directory" | sed 's/-[0-9]\+\(\.[0-9]\+\)*$//')"

	debian/rules clean
	dir0="$(pwd)"

	for i in bionic cosmic
	do
		old_header=$(head -1 ./debian/changelog)
		old_version="$(cat ./debian/changelog | head -n 1 | awk -F "(" '{print $2}' | awk -F ")" '{print $1}')"
		new_version="${old_version}~${i}1"
		sed -i -re "s/${old_version}/${new_version}/g" ./debian/changelog
		sed -i -re "1s/unstable/$i/" ./debian/changelog
		# -I to exclude .git; -d to allow building .changes file without build dependencies installed
		dpkg-buildpackage -I -S -sa -d
		sed  -i -re "1s/.*/${old_header}/" ./debian/changelog
		cd ..
		
		# change PPA names to yours, you may leave only one PPA; I upload to 2 different PPAs at the same time
		#for ppa_name in ppa:mikhailnov/desktop1-dev #ppa:mikhailnov/utils
		for ppa_name in ppa:mikhailnov/utils
		# I can copy from desktop1-dev to utils at Launchpad
		do
			dput -f "$ppa_name" "$(/bin/ls -tr ${pkg_name}_*_source.changes | tail -n 1)"
		done
		
		cd "$dir0"
		sleep 1
	done

	debian/rules clean
	#cd "$dir_start"
}

dir_start="$(pwd)"
for directory in "chromium-browser-68"
do
	cd "$directory"
	build_package #"$directory"
	cd "$dir_start"
done
